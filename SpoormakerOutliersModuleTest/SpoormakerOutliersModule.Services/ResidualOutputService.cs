﻿using SpoormakerOutliersModule.Domain;
using System;
using System.Collections.Generic;
using System.IO;

namespace SpoormakerOutliersModule.Services
{
  public class ResidualOutputService : IResidualOutputService
  {
    public List<ResidualOutput> GetOutliers()
    {
      return new List<ResidualOutput>();
    }

    public List<ResidualOutput> GetResidualOutputs()
    {
      List<ResidualOutput> residualOutputs = new List<ResidualOutput>();
      using (var reader = new StreamReader(@"C:\Costing System Regression Analysis 210706.xlsx"))
      {
        while (!reader.EndOfStream)
        {
          var line = reader.ReadLine();
          var values = line.Split('|');

          residualOutputs.Add(new ResidualOutput(Int32.Parse(values[0]), Int32.Parse(values[1]), Int32.Parse(values[2]), Int32.Parse(values[3])));
        }
      }
      return residualOutputs;
    }
  }
}
