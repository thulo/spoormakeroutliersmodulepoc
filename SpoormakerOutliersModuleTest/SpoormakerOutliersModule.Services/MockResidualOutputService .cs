﻿using SpoormakerOutliersModule.Domain;
using System;
using System.Collections.Generic;
using System.IO;

namespace SpoormakerOutliersModule.Services
{
  public class MockResidualOutputService : IResidualOutputService
  {
    public List<ResidualOutput> GetOutliers(List<ResidualOutput> residualOutputs)
    {
      //Set standard residuals order to descending
      var outliers = new List<ResidualOutput>();
     //First Quartile(Q1)
     //Third Quartile(Q3) 
     //Interquartile Range(IQR)
     //Low outliers = Q1 – 1.5(IQR)
     //High outliers = Q3 + 1.5(IQR) 

      //All Standard residuals outside of this range to be added to the outliers list 

      return outliers;
    }

    public List<ResidualOutput> GetResidualOutputs()
    {
      List<ResidualOutput> residualOutputs = new List<ResidualOutput>();
      residualOutputs.Add(new ResidualOutput(1, 1, 1, 1));
      residualOutputs.Add(new ResidualOutput(4, 4, 4, 4));
      residualOutputs.Add(new ResidualOutput(2, 2, 2, 2));
      residualOutputs.Add(new ResidualOutput(5, 5, 5, 5));
      residualOutputs.Add(new ResidualOutput(3, 3, 3, 3));
      return residualOutputs;

    }

  };

}
