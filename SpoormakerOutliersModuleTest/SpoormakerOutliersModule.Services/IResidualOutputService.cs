﻿using SpoormakerOutliersModule.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpoormakerOutliersModule.Services
{
  public interface IResidualOutputService
  {
    List<ResidualOutput> GetResidualOutputs();
    List<ResidualOutput> GetOutliers(List<ResidualOutput> residualOutputs);
  }
}
