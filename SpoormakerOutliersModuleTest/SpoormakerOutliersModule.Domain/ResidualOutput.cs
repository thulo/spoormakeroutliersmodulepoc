﻿using System;

namespace SpoormakerOutliersModule.Domain
{
  public class ResidualOutput
  {
    private readonly int _observation;
    private readonly int _predictedY;
    private readonly int _residuals;
    private readonly double _standardResiduals;

    public ResidualOutput(int observation, int predictedY, int residuals, double standardResiduals)
    {
      _observation = observation;
      _predictedY = predictedY;
      _residuals = residuals;
      _standardResiduals = standardResiduals;
    }

    public int Observation
    {
      get { return _observation; }
      set { Observation = value; }
    }
    public int PredictedY
    {
      get { return _predictedY; }
      set { PredictedY = value; }
    }
    public int Residuals
    {
      get { return _residuals; }
      set { Residuals = value; }
    }
    public double StandardResiduals
    {
      get { return _standardResiduals; }
      set { StandardResiduals = value; }
    }
  }
}
