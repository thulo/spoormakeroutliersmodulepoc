using SpoormakerOutliersModule.Services;
using System;
using Xunit;

namespace SpoormakerOutliersModuleTest
{
  public class ResidualOutputServiceShould
  {
    private readonly IResidualOutputService _mockResidualOutputService;

    public ResidualOutputServiceShould(IResidualOutputService mockResidualOutputService)
    {
      _mockResidualOutputService = mockResidualOutputService;
    }

    [Fact]
    public void returnResidualOutputs()
    {
      //given - nothing

      //when - I call the service for a list of rsidual output
      var residualOutputs = _mockResidualOutputService.GetResidualOutputs();
      //then - I should git back a list of residual outputs
      Assert.True(residualOutputs.Count > 0, "No residual outputs were returned");
    }

    [Fact]
    public void returnOutliers()
    {
      //given - a list of residual outputs
      var residualOutputs = _mockResidualOutputService.GetResidualOutputs();
      //when - I get the outlying standard residual values 
      var outliers = _mockResidualOutputService.GetOutliers(residualOutputs);
      //then - I get a correct numbers of outliers list of outlying standard residual values 
      Assert.True(outliers.Count == 122, "The incorrect numbers of outliers was returned");

    }
  }
}
